﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossSellingReports
{
    class ApplicationArguments
    {
        public String XmlConfigFile { get; set; }
        public String InputSpreadsheet { get; set; }
    }
}
