﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetGear;
using System.Xml;
using Fclp;
using System.Dynamic;
using System.IO;
using CsvHelper;

namespace CrossSellingReports
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new FluentCommandLineParser<ApplicationArguments>();

            p.SetupHelp("?", "help").WithHeader("This application requires a number of parameters to operate correctly, please review the options below.").Callback(text => Console.WriteLine(text));

            p.Setup(arg => arg.XmlConfigFile).As('x', "xmlconfig").Required().WithDescription("The xml config file in the form:  x={XMLFileName}");
            p.Setup(arg => arg.InputSpreadsheet).As('f', "filename").Required().WithDescription("The input spreadsheet file");
            var options = p.Parse(args);

            if (options.HasErrors == false)
            {
                //Console.WriteLine("Xml file: " + p.Object.XmlConfigFile);
                //Console.WriteLine("Spreadsheet file: " + p.Object.InputSpreadsheet);

                CrossSellingCollator collator = new CrossSellingCollator(p.Object.XmlConfigFile, p.Object.InputSpreadsheet);

                Dictionary<string, Dictionary<string, Tuple<string, string>>> reportData;

                bool allOK = collator.CreateReport(out reportData);

                // Collate a list of results to write out to a CsvFile
                List<dynamic> csvOutput = new List<dynamic>();
                foreach (KeyValuePair<string, Dictionary<string, Tuple<string, string>>> entry in reportData)
                {
                    Console.WriteLine(entry.Key);
                    dynamic lineItem = new ExpandoObject();
                    //lineItem.SoldShippedKey = entry.Key;
                    // This line gets access to the Dynamic properties of the object
                    IDictionary<string, object> underlyingLineItem = lineItem;

                    foreach (KeyValuePair<string, Tuple<string, string>> record in entry.Value)
                    {
                        Console.WriteLine(record.Key + ":");
                        Console.WriteLine(record.Value.Item1);
                        underlyingLineItem.Add(record.Key, record.Value.Item1);
                    }

                    if (csvOutput != null)
                        csvOutput.Add(lineItem);

                    Console.WriteLine("--------------------------------------------------");
                }


                // This will write out the dynamic objects list created from the previous iteration.
                if ((csvOutput != null) &&  (csvOutput.Count > 0) )
                {
                    string dir = GetAppPath();// Path.GetDirectoryName(p.Object.InputSpreadsheet);
                    string csvOutputFile = Path.Combine(dir, "Beam Suntory - Consolidated Delivery Details.csv");
                    if (File.Exists(csvOutputFile))
                        File.Delete(csvOutputFile);

                    using (StreamWriter sw = new StreamWriter(csvOutputFile, false, Encoding.UTF8))
                    {
                        using (CsvWriter csv = new CsvWriter(sw))
                        {
                            csv.WriteRecords(csvOutput);
                        }
                    }
                }
            }
            else
            {
                p.HelpOption.ShowHelp(p.Options);
            }

            Console.WriteLine("Please press any of the keys on the keyboard to Close this application.");
            Console.ReadLine();
        }

        private static string GetAppPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}
