﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SpreadsheetGear;
using System.Collections;
// If we decide to use IValues
//using SpreadsheetGear.Advanced.Cells;

namespace CrossSellingReports
{ 
    internal class CrossSellingCollator
    {
        private string m_reportSpreadsheet;
        private string m_configFile;
        private RowProcessor m_rowProcessor;

        public CrossSellingCollator(string configFile, string reportSpreadsheet)
        {
            if (String.IsNullOrEmpty(configFile))
                throw new ArgumentNullException("configFile");
            if (String.IsNullOrEmpty(reportSpreadsheet))
                throw new ArgumentNullException("reportSpreadsheet");

            // instantiate the object based on supplied xml input file
            m_configFile = configFile;
            m_reportSpreadsheet = reportSpreadsheet;
            m_rowProcessor = new RowProcessor();
            PopulateRowProcessor();
        }

        public bool CreateReport(out Dictionary<string, Dictionary<string, Tuple<string, string>>> reportData)
        {
            reportData = new Dictionary<string, Dictionary<string, Tuple<string, string>>>();

            IWorkbook book = SpreadsheetGear.Factory.GetWorkbook(m_reportSpreadsheet);
            IWorksheet sheet = book.ActiveWorksheet;
            IRange usedRange = sheet.UsedRange;

            Dictionary <string, Dictionary<string, Tuple<string, string> > > collatedData;

            collatedData = new Dictionary<string, Dictionary<string, Tuple<string, string>>>();

            int headerRow = m_rowProcessor.FindHeaderRow(usedRange);
            
            if (headerRow >= 0)
            {
                // TODO:    Look at potentially replacing the current reading approach with the
                //          more performant version using IValues interface.
                //          Would need to understand the row and column offsets of the usedRange.
                //IValues values = (IValues)sheet;

                //Iterate through the data rows collating data as per the config file
                for (int i = headerRow + 1; i < usedRange.RowCount; i++)
                {
                    IRange row = usedRange.Range[i, 0].EntireRow;
                    
                    string uniqueKey = m_rowProcessor.GetUniqueKey(row);

                    if (!reportData.ContainsKey(uniqueKey))     
                    {
                        reportData[uniqueKey] = new Dictionary<string, Tuple<string, string>>();
                    }

                    //iterate through the columns we're interested in 
                    foreach(KeyValuePair<string, int> columnNameIndex in m_rowProcessor.columnNameToIndexMap)
                    {
                        //Get the data 
                        string name = columnNameIndex.Key;
                        int index = columnNameIndex.Value;

                        string type = m_rowProcessor.columnNameToTypeMap[name];
                        string action = m_rowProcessor.columnNameToActionMap[name];

                        string data = "";
                        if (row[0, index].ValueType != SpreadsheetGear.ValueType.Empty)
                        {
                            if (String.Compare(type, "date", true) == 0)
                            {
                                DateTime toFormat = book.NumberToDateTime((double)row[0, index].Value);
                                data = toFormat.ToString("MMM yyyy");
                            } // else data remains the same as it's value
                            else
                            {
                                data = row[0, index].Value.ToString();
                            }
                        }

                        if (data.Contains("\"") || data.Contains("\"\""))
                        {
                            data = m_rowProcessor.FindRepeatedData(usedRange, i, index);
                        }
                        
                        int intData = 0;
                        string newStringData = "";


                        // Prepare or expand upon the dictionary entry
                        if (!reportData[uniqueKey].ContainsKey(name))
                        {
                            reportData[uniqueKey].Add(name, Tuple.Create(data, type));
                        }
                        else
                        {
                            string lcAction = action.ToLower();
                            switch (lcAction)
                            {
                                case "sum":
                                    bool ok = true;
                                    ok = Int32.TryParse(data, out intData);
                                    if (!ok) return false;
                                    int temp = 0;
                                    ok = Int32.TryParse(reportData[uniqueKey][name].Item1, out temp);
                                    if (!ok) return false;
                                    intData = intData + temp;
                                    newStringData = intData.ToString();
                                    break;

                                case "collate":
                                    string stringTemp1 = reportData[uniqueKey][name].Item1;
                                    
                                    //if (!stringTemp1.Contains(data)) // Debug testing
                                    int start = stringTemp1.IndexOf(data);
                                    if(start < 0)
                                    { 
                                        newStringData = stringTemp1 + ", "+ data;
                                    }
                                    else
                                    {
                                        newStringData = stringTemp1; // This was overwriting the previous value with last encountered
                                    }
                                    break;

                                case "record":
                                    newStringData = data;
                                    break;

                                // SP   Allow null and empty to pass through
                                case null:
                                case "":
                                    break;

                                default:
                                    // SP TODO:  Do we allow items we don't recognise, or add a case for empty checking.  We cannot check for null
                                    throw new NotSupportedException("Unuspported Action:  " + lcAction);
                            }

                            reportData[uniqueKey][name] = Tuple.Create(newStringData, type);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Header not found");
                return false;
            }
            
            return true;
        }

        private bool PopulateRowProcessor()
        {
            //Read the xml file
            XmlDocument configFile = new XmlDocument();
            configFile.Load(m_configFile);

            //Read the input spreadsheet

            IRange data = SpreadsheetGear.Factory.GetWorkbook(m_reportSpreadsheet).ActiveWorksheet.UsedRange;
            
            XmlNodeList headers = configFile.GetElementsByTagName("header");

            if(headers.Count > 1)
            {
                //Raise error
                Console.WriteLine("More than one header definition found - this is invalid.");
                return false;
            }
            else if(headers.Count < 1)
            {
                //Raise error
                Console.WriteLine("No header definition found");
                return false;
            }

            XmlNode header = headers[0];

            IEnumerator ienum = header.GetEnumerator();

            XmlNode column;

            while(ienum.MoveNext())
            {
                column = (XmlNode)ienum.Current;

                //Fill in the column index
                string columnName = column.SelectNodes("name")[0].ChildNodes[0].Value;
                IRange foundCell = data.Find(columnName, null, FindLookIn.Values, LookAt.Part, SearchOrder.ByColumns, SearchDirection.Next, false);
                
                //if name isn't found search for alternate names.
                if (foundCell != null)
                {
                    m_rowProcessor.columnNameToIndexMap.Add(columnName, foundCell.Column);
                }
                else
                {
                    string columnNameAlt = column.SelectNodes("alternate-name")[0].ChildNodes[0].Value;
                    if (String.IsNullOrEmpty(columnNameAlt))
                    {
                        throw new ArgumentNullException("columnNameAlt");
                    }
                    else
                    {
                        foundCell = data.Find(columnNameAlt, null, FindLookIn.Values, LookAt.Part, SearchOrder.ByColumns, SearchDirection.Next, false);
                        if (foundCell == null)
                            throw new ArgumentNullException("foundCell");
                        else
                            m_rowProcessor.columnNameToIndexMap.Add(columnName, foundCell.Column);
                    }
                }

                //SKP TODO: better validation here (report error if action or type aren't found.
                string action = column.SelectNodes("action")[0].ChildNodes[0].Value;
                m_rowProcessor.columnNameToActionMap.Add(columnName, action);

                if (column.SelectNodes("formsuniquekey").Count > 0)
                {
                    m_rowProcessor.uniqueKeyColumnIndices.Add(foundCell.Column);
                }

                string type = column.SelectNodes("type")[0].ChildNodes[0].Value;
                m_rowProcessor.columnNameToTypeMap.Add(columnName, type);
            }
            
            //set up the row processor
            return false;
        }
    }
}