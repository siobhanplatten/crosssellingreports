﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetGear;

namespace CrossSellingReports
{
    internal class RowProcessor
    {
        public RowProcessor()
        {
            uniqueKeyColumnIndices = new HashSet<int>();
            columnNameToIndexMap = new Dictionary<string, int>();
            columnNameToActionMap = new Dictionary<string, string>();
            columnNameToTypeMap = new Dictionary<string, string>();
        }

        public Dictionary<string, int> columnNameToIndexMap;
        public Dictionary<string, string> columnNameToActionMap;
        public HashSet<int> uniqueKeyColumnIndices;
        public Dictionary<string, string> columnNameToTypeMap;
        
        public string GetUniqueKey(IRange row)
        {
            string uniqueKey = null;

            foreach (int index in uniqueKeyColumnIndices)
            {
                //Concatenate string representing unique key for the row here.
                if ((row[0, index].ValueType != SpreadsheetGear.ValueType.Empty) && (row[0, index].Value != null))
                {     
                    string cellContents = row[0, index].Value.ToString();
                    uniqueKey += cellContents;
                    uniqueKey += ":";
                }
            }

            return uniqueKey;
        } 

        public int FindHeaderRow(IRange usedRange)
        {
            //Get a column name, we don't care which one
            //Find it in the report spreadsheet and return its row  number
            KeyValuePair<string, int> columnEntry = columnNameToIndexMap.First();

            IRange cell = usedRange.Find(columnEntry.Key, null, FindLookIn.Values, LookAt.Part, SearchOrder.ByColumns, SearchDirection.Next, false);
           
            return cell.Row;
        }

        public string FindRepeatedData(IRange usedRange, int row, int column)
        {
            int headerRow = FindHeaderRow(usedRange);

            for( int i = row; i > headerRow; i--)
            {
                //if(usedRange[i, column].Value.ToString() != "\"" && usedRange[row, column].Value.ToString() != "\"\"")
                if (usedRange[i, column].ValueType != SpreadsheetGear.ValueType.Empty)
                {
                    return usedRange[i, column].Value.ToString();
                }
            }

            return "\"";
        }
    }
}